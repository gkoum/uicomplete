class TopologiesController < ApplicationController
  before_action :set_topology, only: [:show, :edit, :update, :destroy]

  # GET /topologies
  # GET /topologies.json
  def index
    @topologies = Topology.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @topologies }
    end
  end

  # GET /topologies/1
  # GET /topologies/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @topology }
    end
  end

  # GET /topologies/new
  def new
    @topology = Topology.new
  end

  # GET /topologies/1/edit
  def edit
  end

  # POST /topologies
  # POST /topologies.json
  def create
    @topology = Topology.new(topology_params)

    respond_to do |format|
      if @topology.save
        format.html { redirect_to @topology, notice: 'Topology was successfully created.' }
        format.json { render json: @topology, status: :created }
      else
        format.html { render action: 'new' }
        format.json { render json: @topology.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topologies/1
  # PATCH/PUT /topologies/1.json
  def update
    respond_to do |format|
      if @topology.update(topology_params)
        format.html { redirect_to @topology, notice: 'Topology was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @topology.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topologies/1
  # DELETE /topologies/1.json
  def destroy
    @topology.destroy
    respond_to do |format|
      format.html { redirect_to topologies_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topology
      @topology = Topology.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topology_params
      params.permit(:name, :topology_type)
    end
end
