class SessionsController < Devise::SessionsController
  before_action :warden_authenticate

  def create
    sign_in(resource_name, resource)
    resource.reset_authentication_token!
    flash[:notice] = "You have been signed in."
    redirect_to root_url
  end

  def destroy
    sign_out(resource_name)
    resource.clear_authentication_token!
    flash[:notice] = "You have been signed out."
    redirect_to new_user_session_path
  end

  private

  def warden_authenticate
    self.resource = warden.authenticate!(auth_options)
  end
end
