json.array!(@topologies) do |topology|
  json.extract! topology, :id, :name, :topology_type
  json.url topology_url(topology, format: :json)
end
