class TopologySerializer < ActiveModel::Serializer
  attributes :id, :name, :topology_type
end
