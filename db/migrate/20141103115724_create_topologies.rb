class CreateTopologies < ActiveRecord::Migration
  def change
    create_table :topologies do |t|
      t.string :name
      t.string :topology_type

      t.timestamps
    end
  end
end
