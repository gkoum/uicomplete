Todo::Application.routes.draw do
  resources :topologies

  devise_for :users  

  namespace :api, defaults: {format: :json} do
    # devise_scope :user do
    #   resource :session, only: [:create, :destroy]
    # end
    resources :task_lists, only: [:index, :create, :update, :destroy, :show] do
      resources :tasks, only: [:index, :create, :update, :destroy]
    end
  end

  devise_scope :user do
     resource :session, only: [:create, :destroy]
  end
  root :to => "templates#test"
  
  # devise_scope :user do 
  #     get '/users/sign_out' => 'sessions#new'
  # end
  get '/ajax/dashboard' => 'ajax#dashboard'
  get '/ajax/maps' => 'ajax#maps'
  get '/ajax/gallery_simple' => 'ajax#gallery_simple'
  get '/ajax/typography' => 'ajax#typography'
  get '/ajax/map_fullscreen' => 'ajax#map_fullscreen'
  get '/ajax/ui_grid' => 'ajax#ui_grid'
  get '/page_login' => 'ajax#page_login'

  get '/dashboard' => 'templates#index'
  get '/task_lists/:id' => 'templates#index'
  get '/templates/:path.html' => 'templates#template', :constraints => { :path => /.+/  }
end
